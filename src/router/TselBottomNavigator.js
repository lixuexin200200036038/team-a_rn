import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { Icon } from 'react-native-elements';

import { Platform } from 'react-native';

import { Colors } from 'utils/ColorReferences';

import { MyAccounts } from '../features/both/';
// import { MenuInvoices } from "../features/corporate";
import { MenuHome, MenuCorporates, MenuOrders } from '../features/telkomsel';


const Tab = createBottomTabNavigator();

const BottomNavigator = (props) => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      sceneContainerStyle={{
        backgroundColor: '#fff0'
      }}
      tabBarOptions={{
        showLabel: false,
        style: {
          backgroundColor: Colors.gray800,
          paddingTop: Platform.OS === 'ios' ? 20 : 0,
          borderTopRightRadius: Platform.OS === 'ios' ? 38 : 0,
          borderTopLeftRadius: Platform.OS === 'ios' ? 38 : 0
        }
      }}
      screenOptions={({ route }) => ({
        tabBarBadgeStyle: {
          color: Colors.red700
        },
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          size = focused ? 30 : 22;
          color = focused ? Colors.red700 : Colors.white;
          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'Corporates') {
            iconName = focused ? 'briefcase' : 'briefcase-outline';
          } else if (route.name === 'Orders') {
            iconName = focused ? 'receipt' : 'receipt-outline';
          } else if (route.name === 'My Account') {
            iconName = focused ? 'person' : 'person-outline';
          }
          return (
            <Icon name={iconName} type="ionicon" color={color} size={size} />
          );
        }
      })}
    >
      <Tab.Screen name="Home" component={MenuHome} />
      <Tab.Screen name="Corporates" component={MenuCorporates} />
      <Tab.Screen name="Orders" component={MenuOrders} />
      <Tab.Screen name="My Account" component={MyAccounts} />
    </Tab.Navigator>
  );
};

export default BottomNavigator;

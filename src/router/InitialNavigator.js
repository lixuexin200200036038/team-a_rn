import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Login, Splash } from '../features/both/';

import TselBottomNavigator from './TselBottomNavigator';
import CorpBottomNavigator from './CorpBottomNavigator';
// import { MenuHome, MenuInvoices, MenuOrders } from "../features/corporate";
// import { MenuHome, MenuCorporates, MenuOrders } from "../features/telkomsel";

const Stack = createStackNavigator();

const InitialNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen
        name="TselBottomNavigator"
        component={TselBottomNavigator}
      />
      <Stack.Screen
        name="CorpBottomNavigator"
        component={CorpBottomNavigator}
      />
    </Stack.Navigator>
  );
};

export default InitialNavigator;

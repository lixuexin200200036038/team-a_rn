import {
  LOGIN_SUCCESS,
  LOGIN_LOADING,
  LOGIN_FAILED,
  STORE_DATA_SUCCESS,
  STORE_DATA_LOADING,
  STORE_DATA_FAILED,
  LOGOUT_ACTION
} from 'reduxs/actions/AuthAction';

const initialState = {
  isLoading: false,
  session: {
    isLoggedIn: false,
    role: null,
    token: null,
    data: null
  }
};

function AuthReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: action.isLoading,
        session: action.payload
      };
    case LOGIN_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case LOGIN_FAILED:
      return {
        ...state,
        isLoading: action.isLoading,
        session: initialState.session
      };
    case STORE_DATA_SUCCESS:
      return {
        ...state,
        isLoading: action.isLoading,
        session: action.payload
      };
    case STORE_DATA_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    case STORE_DATA_FAILED:
      return {
        ...state,
        isLoading: action.isLoading,
        session: initialState.session
      };
    case LOGOUT_ACTION:
      return {
        ...state,
        session: initialState.session
      };
    default:
      return state;
  }
}

export default AuthReducer;

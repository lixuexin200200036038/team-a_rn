import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  cardTickets: {
    backgroundColor: Colors.white700,
    borderRadius: 5,
    borderColor: Colors.gray700,
    borderWidth: 0.5,
    marginLeft: 20,
    padding: 10,
    width: 200
  },
  headWrap: {
    flexDirection: 'row',
    marginBottom: 5
  },
  icon: {
    marginLeft: 5
  },
  titleHead: {
    marginTop: 3,
    marginLeft: 10
  },
  title: {
    color: Colors.red900,
    fontWeight: 'bold'
  },
  subtitle: {
    color: Colors.red900,
    fontWeight: 'bold'
  },
  textTicket: {
    fontSize: 12,
    marginLeft: 12,
    marginRight: 10
  },
  reply: {
    color: 'blue',
    marginLeft: 12,
    marginVertical: 5,
    fontSize: 16
  }
});

import { StyleSheet } from 'react-native';

import { Colors } from '../../utils/ColorReferences';

export const styles = StyleSheet.create({
  button: (type) => ({
    backgroundColor: type === 'secondary' ? Colors.gray700 : Colors.red700,
    borderRadius: 5
  }),
  textButton: {
    fontSize: 16,
    fontWeight: '800',
    textAlign: 'center',
    color: 'white',
    paddingVertical: 12,
    alignSelf: 'center',
    lineHeight: 18
  }
});

import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from '../../utils/ColorReferences';

import { styles } from './CardBillsAMStyle';

const CardBillsAM = (props) => {
  return (
    <View style={styles.cardInvoice}>
      <View style={styles.invoiceWrap}>
        <Text style={styles.invoice}>Invoice Tsel-101/II/2021</Text>
        <Text style={styles.month}>Feb 2021</Text>
        <Text style={styles.month}>PT. Fakhaza Healthy Kitchen</Text>
        <Text style={styles.amount}>Total: Rp. 1.700.000</Text>
      </View>
      <View style={styles.statusWrap}>
        <Text style={styles.status}>Paid</Text>
        <TouchableOpacity>
          <Icon
            name="chevron-right"
            style={styles.icon}
            size={25}
            color={Colors.gray700}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CardBillsAM;

import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'react-native';

import { styles } from './style';



const CardOrderCorporate = ({ date, Jumlah, Package, Summary, Status, onPress, isAddNumber }) => {
  return (
    <TouchableOpacity onPress={onPress} disabled={isAddNumber}>

      <View style={styles.container}>
        <View>

          <Text style= {styles.textred}> {date}</Text>
          { isAddNumber ?
            <>
              <Text style= {styles.textgrey}>Quantity : {Jumlah}</Text>
              <Text style= {styles.textgrey}>Package : {Package}</Text>
            </>
            : null }
          <Text style= {styles.textRenewal}> {Summary}</Text>
        </View>
        <View style={styles.textWrapper}>
          <Text style= {styles.textStatus}> {Status}</Text>

        </View>

      </View>


    </TouchableOpacity>
  );
};


export default CardOrderCorporate;


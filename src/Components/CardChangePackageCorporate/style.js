import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  container: {

    marginHorizontal: 2,
    marginVertical: 5,
    padding: 2,
    borderWidth: 1,
    borderColor: Colors.gray700,
    borderRadius: 20,
    width: 324,
    height: 104
  },

  textRenewal:
  {
    color: Colors.red700,
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 5

  },
  container1: {
    flexDirection: 'row',

    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,


    borderRadius: 5,
    width: 139,
    height: 34,
    justifyContent: 'center'

  },

  container2: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 10

  }




});

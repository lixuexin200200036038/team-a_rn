import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { styles } from './CardCorparateStyle';

const CardCorporate = ({ Corporate, gambar, judul, Nama, Email, Phone, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Text style={styles.textred}> {Corporate}</Text>
        <View style={styles.containerContent}>
          <Image
            source={{
              uri: gambar
            }}
            style={styles.image}
          />
          <View style={styles.textWrapper}>
            <Text style={styles.pic}> {judul}</Text>
            <Text style={styles.textgrey}> {Nama}</Text>
            <Text style={styles.textgrey}> {Email}</Text>
            <Text style={styles.textgrey}> {Phone}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default CardCorporate;

import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import { styles } from './CardOrderDetailStyle';



const CardOrderDetail = ({ nomor, paket, orderType, renewal, status, onPressApprove, onPressReject }) => {
  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.textRenewal}>{nomor}</Text>
        <Text style={styles.textred}>{paket}</Text>
        <Text style={styles.textgrey}>{orderType}</Text>
        <Text style={styles.textRenewal}>{renewal}</Text>
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.textred1}> Status: </Text>
        <Text style={styles.textStatus}> {status}</Text>

        { status === 'open' ?
          <View style={styles.container2}>
            <TouchableOpacity style={styles.container1} onPress={onPressApprove}>
              <Text style={styles.status}>Approve</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.container1} onPress={onPressReject}>
              <Text style={styles.status}>Reject</Text>
            </TouchableOpacity>
          </View>
          : null }
      </View>
    </View>
  );};

export default CardOrderDetail;

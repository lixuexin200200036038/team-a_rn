import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  header: {
    marginTop: 60,
    fontSize: 28,
    textAlign: 'center'
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#8492A6',
    borderRadius: 10
  },
  Corporate: {
    justifyContent: 'center',
    marginLeft: 20
  },
  textWrapper: {
    justifyContent: 'center'
  },
  textContainer: {
    marginLeft: 5
  },
  textred:
  {
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 10
  },
  textgrey:
  {
    color: Colors.gray700,
    fontSize: 17
  },
  textRenewal:
  {
    color: Colors.red700,
    fontSize: 17,
    fontWeight: 'bold',
    marginVertical: 5
  },
  textStatus:
  {
    textAlign: 'center',
    color: Colors.red700,
    fontSize: 17,
    marginBottom: 5,
    fontWeight: 'bold'
  },
  textred1:
  {
    color: Colors.red700,
    fontSize: 17,
    textAlign: 'center',
    marginVertical: 20
  },
  container1: {
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5,
    width: 90,
    height: 50
  },
  status: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  container2: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 10
  },
  container3: {
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5
  }
});

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Alert } from 'react-native';

export const setSharedPreferences = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    Alert.alert('Damn!', `Error: ${e.message}`);
  }
};

export const getSharedPreferences = async (key, defaults = '') => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
    return defaults;
  } catch (e) {
    Alert.alert('Damn!', `Error: ${e.message}`);
  }
};

export const setSharedPreferencesJson = async (key, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
  } catch (e) {
    Alert.alert('Damn!', `Error: ${e.message}`);
  }
};

export const getSharedPreferencesJson = async (key, defaults = '') => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : defaults;
  } catch (e) {
    Alert.alert('Damn!', `Error: ${e.message}`);
  }
};

// getAllKeys = async () => {
//   let keys = [];
//   try {
//     keys = await AsyncStorage.getAllKeys();
//   } catch (e) {
//     // read key error
//   }

//   console.log(keys);
//   // example console.log result:
//   // ['@MyApp_user', '@MyApp_key']
// };

// getMultiple = async () => {
//   let values;
//   try {
//     values = await AsyncStorage.multiGet(['@MyApp_user', '@MyApp_key']);
//   } catch (e) {
//     // read error
//   }
//   console.log(values);

//   // example console.log output:
//   // [ ['@MyApp_user', 'myUserValue'], ['@MyApp_key', 'myKeyValue'] ]
// };

// multiSet = async () => {
//   const firstPair = ['@MyApp_user', 'value_1'];
//   const secondPair = ['@MyApp_key', 'value_2'];
//   try {
//     await AsyncStorage.multiSet([firstPair, secondPair]);
//   } catch (e) {
//     //save error
//   }

//   console.log('Done.');
// };

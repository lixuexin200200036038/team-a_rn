import React, { useEffect, useState, useRef } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Modal,
  TextInput,
  ScrollView,
  RefreshControl,
  Alert
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import Button from 'components/Button/Button';

import { useSelector } from 'react-redux';

import BottomSheet from 'reanimated-bottom-sheet';

import { numberWithDots } from 'utils/helper';
import { Images } from 'utils/ImageReferences';
import { Colors } from 'utils/ColorReferences';

import CardTicketCorporate from 'components/CardTicketCorporate';

import {
  getMsisdnCorporate,
  getPackage,
  changePackageCorporate,
  addNumberCorporate,
  createTicketCorporate,
  getTicketCorporate,
  getCurrentBillCorporate
} from 'engine/ApiProcessors';

import { styles } from './style';

import { itemStyles } from './itemStyle';

const Home = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const [InitialMsisdnData, setInitialMsisdnData] = useState([]);
  const [msisdnData, setMsisdnData] = useState([]);
  const [packageData, setPackageData] = useState([]);
  const [modalChangePackages, setModalChangePackages] = useState(false);
  const [modalAddNumber, setModalAddNumber] = useState(false);
  const [quantityAddNumber, setQuantityAddNumber] = useState('');
  const [selectedPackageAddNumber, setSelectedPackageAddNumber] = useState(-1);
  const [changePackageData, setChangePackageData] = useState([]);
  const [activeMsisdn, setActiveMsisdn] = useState(null);
  const [headlineTicket, setHeadlineTicket] = useState('');
  const [selectedTicketCategory, setSelectedTicketCategory] = useState('Teknis');
  const [modalCreateTicket, setModalCreateTicket] = useState(false);
  const [ticketData, setTicketData] = useState([]);
  const [currentBillData, setCurrentBillData] = useState(0);
  const [refreshing] = useState(false);

  const getData = () => {
    getMsisdnCorporate(session.data.pic_id, session.token)
      .then((result) => {
        setInitialMsisdnData(result.data.data);
        setMsisdnData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching MSISDN: ${err.toString()}`);
      });

    getPackage(session.token)
      .then((result) => {
        setPackageData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching package data: ${err.toString()}`);
      });

    getTicketCorporate(session.token)
      .then((result) => {
        setTicketData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching ticket data: ${err.toString()}`);
      });

    getCurrentBillCorporate(session.token)
      .then((result) => {
        setCurrentBillData(result.data.data[0].total_invoice);
      })
      .catch((err) => {
        Alert.alert(`Error fetching bill data: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getData();
  }, []);

  const sendChangePackageRequest = () => {
    if (changePackageData.length > 0) {
      changePackageCorporate(changePackageData, session.token)
        .then((result) => {
          setModalChangePackages(false);
          setChangePackageData([]);
          setMsisdnData(InitialMsisdnData);
          // sheetRef.current.snapTo(1);
          Alert.alert(result.data.message);
        })
        .catch((err) => {
          Alert.alert(`Error changing packages: ${err.toString()}`);
        });
    } else {
      setModalChangePackages(false);
    }
  };

  const renderChangePackage = ({ item }) => {
    const renderItemPackage = (itemPackage) => {
      const itemPackageContent = itemPackage.item;

      const removePackageFromMsisdn = () => {
        const tempMsisdnData = msisdnData.map((msisdnDatum) => {
          if (msisdnDatum.msisdn === item.msisdn) {
            msisdnDatum.packages = msisdnDatum.packages.filter(
              (packageItem) =>
                packageItem.package_id !== itemPackageContent.package_id
            );
          }
          return msisdnDatum;
        });

        setMsisdnData(tempMsisdnData);

        const found = changePackageData.find(
          (changePackageDatum) => changePackageDatum.msisdn === item.msisdn
        );

        if (found) {
          found.package_remove.push({
            package_id: itemPackageContent.package_id
          });
        } else {
          changePackageData.push({
            msisdn: item.msisdn,
            package_remove: [{ package_id: itemPackageContent.package_id }],
            package_new: []
          });
        }
      };

      return (
        <View style={itemStyles.container1}>
          <Text style={itemStyles.status}>
            {' '}
            {itemPackageContent.package_name}
          </Text>
          <TouchableOpacity onPress={() => removePackageFromMsisdn()}>
            <Image style={itemStyles.trash} source={Images.trash} />
          </TouchableOpacity>
        </View>
      );
    };

    return (
      <View style={itemStyles.container}>
        <Text style={itemStyles.textRenewal}> {item.msisdn}</Text>
        <View style={itemStyles.container2}>
          <FlatList
            data={item.packages}
            renderItem={renderItemPackage}
            keyExtractor={(_, index) => index.toString()}
          />
        </View>
        <TouchableOpacity
          style={itemStyles.container3}
          onPress={() => {
            setActiveMsisdn(item.msisdn);
            sheetRef.current.snapTo(0);
          }}
        >
          <Text style={itemStyles.status}>Add Package</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const renderAddPackage = ({ item }) => {
    const addPackageToMsisdn = () => {
      let foundPackages = false;

      const tempMsisdnData = msisdnData.map((msisdnDatum) => {
        if (msisdnDatum.msisdn === activeMsisdn) {
          const found = msisdnDatum.packages.find(
            (packageItem) => packageItem.package_id === item.package_id
          );
          if (found) {
            foundPackages = true;
            return msisdnDatum;
          } else {
            msisdnDatum.packages.push(item);
          }
        }
        return msisdnDatum;
      });

      setMsisdnData(tempMsisdnData);

      if (!foundPackages) {
        const found = changePackageData.find(
          (changePackageDatum) => changePackageDatum.msisdn === activeMsisdn
        );

        if (found) {
          found.package_new.push({ package_id: item.package_id });
        } else {
          changePackageData.push({
            msisdn: activeMsisdn,
            package_remove: [],
            package_new: [{ package_id: item.package_id }]
          });
        }
      }

      sheetRef.current.snapTo(1);
    };

    return (
      <TouchableOpacity
        style={itemStyles.container1}
        onPress={() => addPackageToMsisdn()}
      >
        <Text style={itemStyles.status}> {item.package_name}</Text>
      </TouchableOpacity>
    );
  };

  const sendAddNumberRequest = () => {
    addNumberCorporate(
      { quantity: quantityAddNumber, package_id: selectedPackageAddNumber },
      session.token
    )
      .then((result) => {
        Alert.alert('Number added successfully');
        setModalAddNumber(false);
        setSelectedPackageAddNumber(-1);
        setQuantityAddNumber('');
      })
      .catch((err) => {
        Alert.alert(`Error adding number: ${err.toString()}`);
      });
  };

  const validateInputAddNumber = () => {
    if (isNaN(quantityAddNumber) || isNaN(parseFloat(quantityAddNumber))) {
      Alert.alert('Please input a number in the quantity field');
    } else if (selectedPackageAddNumber === -1) {
      Alert.alert('Please select a package');
    } else if (parseInt(quantityAddNumber) < 1) {
      Alert.alert('Quantity must be bigger than 0');
    } else {
      sendAddNumberRequest();
    }
  };

  const sendCreateTicketRequest = () => {
    createTicketCorporate(
      { headline: headlineTicket, ticket_category: selectedTicketCategory },
      session.token
    )
      .then((result) => {
        Alert.alert('Ticket created successfully');
        setModalCreateTicket(false);
        setHeadlineTicket('');
        setSelectedTicketCategory('Teknis');
      })
      .catch((err) => {
        Alert.alert(`Error creating ticket: ${err.toString()}`);
      });
  };

  const validateInputCreateTicket = () => {
    if (headlineTicket.length < 1) {
      Alert.alert('Please input a text in the headline field');
    } else {
      sendCreateTicketRequest();
    }
  };

  const TicketDetail = (ticket) => {
    props.navigation.navigate('TicketDetailCorporate', {
      ticket
    });
  };

  const renderTicketData = ({ item }) => {
    return (
      <CardTicketCorporate
        title={item.ticket_category}
        status={item.status}
        content={item.headline}
        onPress={() => TicketDetail(item)}
      />
    );
  };

  const sheetRef = useRef(null);

  return (
    <>
      <ScrollView
        style={styles.mainPage}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={getData} />
        }
      >
        <View style={styles.account}>
          <View style={styles.profilePicturePosition}>
            <Image source={Images.profile} style={styles.profilePicture} />
          </View>
          <View style={styles.detailProfile}>
            <Text style={styles.name}>{session.data && session.data.name}</Text>
            <Text style={styles.company}>
              {session.data && session.data.job_position}
            </Text>
          </View>
        </View>

        <View style={styles.monthly}>
          <Text style={styles.usage}>Monthly Usage</Text>
          <Text style={styles.amount}>
            Rp. {numberWithDots(currentBillData)}
          </Text>
        </View>

        <View style={styles.button}>
          <Button
            title="Add New Numbers"
            onPress={() => {
              setModalAddNumber(true);
            }}
          />
        </View>

        <View style={styles.button}>
          <Button
            title="Change Packages to Numbers"
            onPress={() => {
              setModalChangePackages(true);
              setChangePackageData([]);
            }}
          />
        </View>

        <Text style={styles.tickets}>Tickets</Text>

        <View style={styles.button}>
          <Button
            title="Create Ticket"
            onPress={() => {
              setModalCreateTicket(true);
            }}
          />
        </View>

        <View>
          <FlatList
            horizontal
            data={ticketData}
            keyExtractor={(item, index) => 'key' + index}
            renderItem={renderTicketData}
          />
        </View>

        <Modal visible={modalChangePackages} animationType="slide" transparent>
          <View style={styles.layar}>
            <View style={styles.container}>
              <Text style={styles.textgrey}>Change Packages</Text>
              <TouchableOpacity
                onPress={() => {
                  setModalChangePackages(false);
                  setMsisdnData(InitialMsisdnData);
                  setChangePackageData([]);
                  sheetRef.current.snapTo(1);
                }}
              >
                <Text style={styles.textgrey}> X</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              data={msisdnData}
              renderItem={renderChangePackage}
              keyExtractor={(_, index) => index.toString()}
            />
            <View style={{ justifyContent: 'center', marginHorizontal: 30 }}>
              <View style={{ marginTop: 10 }}>
                <Button
                  title="Apply"
                  onPress={() => sendChangePackageRequest()}
                />
              </View>
            </View>
          </View>

          <BottomSheet
            ref={sheetRef}
            initialSnap={1}
            snapPoints={[300, 0]}
            borderRadius={10}
            renderContent={() => (
              <View style={styles.layar3}>
                <View style={styles.container}>
                  <Text style={styles.textgrey}>Add Packages</Text>
                  <TouchableOpacity
                    onPress={() => {
                      sheetRef.current.snapTo(1);
                    }}
                  >
                    <Text style={styles.textgrey}> X</Text>
                  </TouchableOpacity>
                </View>
                <FlatList
                  data={packageData}
                  renderItem={renderAddPackage}
                  keyExtractor={(_, index) => index.toString()}
                />
              </View>
            )}
          />
        </Modal>

        <Modal visible={modalAddNumber} animationType="slide" transparent>
          <View style={styles.layar2}>
            <View style={styles.container}>
              <Text style={styles.textRenewal1}> Add Numbers</Text>
              <TouchableOpacity
                onPress={() => {
                  setModalAddNumber(false);
                  setSelectedPackageAddNumber(-1);
                  setQuantityAddNumber('');
                }}
              >
                <Text style={styles.textgrey}> X</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.textRenewal}> Quantity</Text>
            <View style={styles.textInputContainer}>
              <TextInput
                label="Jumlah PCS"
                style={styles.textInput}
                keyboardType="numeric"
                value={quantityAddNumber.toString()}
                onChangeText={(number) => setQuantityAddNumber(number)}
              />
            </View>
            <Text style={styles.textRenewal}> Package</Text>
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={selectedPackageAddNumber}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedPackageAddNumber(itemValue)
                }
                style={styles.picker}
              >
                {packageData.map((packageDatum) => (
                  <Picker.Item
                    label={packageDatum.package_name}
                    key={packageDatum.package_id.toString()}
                    value={packageDatum.package_id}
                  />
                ))}
              </Picker>
            </View>

            <View
              style={{
                justifyContent: 'center',
                marginHorizontal: 30,
                marginVertical: 20
              }}
            >
              <View>
                <Button
                  title="Add Numbers"
                  onPress={() => validateInputAddNumber()}
                />
              </View>
            </View>
          </View>
        </Modal>

        <Modal visible={modalCreateTicket} animationType="slide" transparent>
          <View style={styles.layar2}>
            <View style={styles.container}>
              <Text style={styles.textRenewal1}> Create Ticket</Text>
              <TouchableOpacity
                onPress={() => {
                  setModalCreateTicket(false);
                  setHeadlineTicket('');
                  setSelectedTicketCategory('Teknis');
                }}
              >
                <Text style={styles.textgrey}> X</Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.textRenewal}> Headline</Text>
            <View style={styles.textInputContainer}>
              <TextInput
                style={styles.textInput}
                value={headlineTicket}
                onChangeText={(text) => setHeadlineTicket(text)}
              />
            </View>
            <Text style={styles.textRenewal}> Category</Text>
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={selectedTicketCategory}
                onValueChange={(itemValue, itemIndex) =>
                  setSelectedTicketCategory(itemValue)
                }
                style={styles.picker}
              >
                <Picker.Item label="Teknis" value="Teknis" />
                <Picker.Item label="Non-teknis" value="Non-teknis" />
              </Picker>
            </View>

            <View
              style={{
                justifyContent: 'center',
                marginHorizontal: 30,
                marginVertical: 20
              }}
            >
              <View>
                <Button
                  title="Add Numbers"
                  onPress={() => validateInputCreateTicket()}
                />
              </View>
            </View>
          </View>
        </Modal>
      </ScrollView>

      <TouchableOpacity
        style={{
          width: 58,
          height: 58,
          borderWidth: 0.5,
          borderColor: Colors.red700,
          alignItems: 'center',
          justifyContent: 'center',
          position: 'absolute',
          bottom: 16,
          right: 18,
          backgroundColor: '#fff',
          borderRadius: 100
        }}
        onPress={() => {
          let url = `http://139.59.124.53:4581/api/chat?username=${session.data.email}&room=${session.data.email}`;
          props.navigation.navigate('ChatDetail', url);
        }}
      >
        <Icon name="chatbubble-outline" size={30} color={Colors.red700} />
      </TouchableOpacity>
    </>
  );
};

export default Home;

import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  mainPage: {
    backgroundColor: Colors.white700
  },
  account: {
    flexDirection: 'row',
    backgroundColor: '#74b9ff',
    padding: 20,
    borderWidth: 0.2,
    borderColor: Colors.gray700
  },
  profilePicturePosition: {
    alignSelf: 'center'
  },
  profilePicture: {
    height: 60,
    width: 60,
    borderRadius: 100
  },
  detailProfile: {
    marginLeft: 20,
    alignSelf: 'center',
    marginBottom: 10
  },
  name: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: 'bold'
  },
  company: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: 'bold',
    marginTop: 5
  },
  accounManager: {
    color: Colors.red700,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 20
  },
  phone: {
    color: Colors.gray700,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    marginTop: 5
  },
  email: {
    color: Colors.gray700,
    alignSelf: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    marginBottom: 10
  },
  button: {
    width: 250,
    alignSelf: 'center',
    marginVertical: 10
  },
  monthly: {
    alignSelf: 'center',
    borderWidth: 0.3,
    borderRadius: 10,
    width: 320,
    alignItems: 'center',
    marginVertical: 20
  },
  usage: {
    color: Colors.red700,
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 5
  },
  amount: {
    color: Colors.red700,
    fontSize: 16,
    marginBottom: 5
  },
  tickets: {
    fontSize: 16,
    color: Colors.red700,
    fontWeight: 'bold',
    marginLeft: 20,
    marginVertical: 10
  },
  chatButton: {
    height: 50,
    width: 50,
    borderRadius: 50,
    borderWidth: 0.3,
    position: 'absolute',
    bottom: 10,
    right: 10,
    borderColor: Colors.gray700
  },
  icon: {
    alignSelf: 'center',
    marginVertical: 12
  },
  layar: {
    height: '80%',
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    padding: 10
  },
  layar2: {
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    borderWidth: StyleSheet.hairlineWidth
  },
  layar3: {
    height: '80%',
    backgroundColor: 'white',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    padding: 10
  },
  textgrey: {
    color: Colors.gray700,
    fontSize: 21,
    fontWeight: 'bold',
    marginLeft: 50
  },
  container: {
    flexDirection: 'row',
    padding: 15,
    justifyContent: 'flex-end'
  },
  textRenewal: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 20,
    marginVertical: 10
  },
  textRenewal1: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 90
  },
  pickerContainer: {
    borderWidth: 1,
    margin: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  picker: {
    width: '100%'
  },
  textInputContainer: {
    borderWidth: 1,
    margin: 10
  },
  textInput: {
  }
});

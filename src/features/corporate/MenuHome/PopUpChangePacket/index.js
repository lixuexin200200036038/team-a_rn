import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'react-native';
import { SearchBar } from 'react-native-elements';

import { styles } from './style';

const ChangePacketScreen = (props) => {
  const back = () => {
    props.navigation.navigate('Corporates');
  };

  return (
    <View>
      <View style={styles.header}>
        <View style={styles.container}>
          <Text style={styles.textRenewal}> Change Packeges</Text>
          <TouchableOpacity onPress={() => back()}>
            <Text style={styles.textgrey}> X</Text>
          </TouchableOpacity>
        </View>
        <SearchBar placeholder="Search Number" />
      </View>
    </View>
  );
};

export default ChangePacketScreen;

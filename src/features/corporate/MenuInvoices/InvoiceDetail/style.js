import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  mainPage: {
    backgroundColor: Colors.white700,
    flex: 1
  },
  textHeader: {
    color: Colors.red700,
    fontSize: 20,
    fontWeight: 'bold'
  },
  containerHeader: {
    backgroundColor: Colors.white700,
    justifyContent: 'space-around',
    borderBottomColor: Colors.gray700
  },
  summary: {
    fontSize: 20,
    color: Colors.red900,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 10
  },
  status: {
    fontSize: 14,
    color: Colors.red700,
    fontWeight: 'bold',
    marginTop: 5,
    marginLeft: 15
  },
  month: {
    fontSize: 14,
    color: Colors.red700,
    fontWeight: 'bold',
    marginTop: 5,
    marginLeft: 15
  },
  company: {
    fontSize: 14,
    color: Colors.red700,
    fontWeight: 'bold',
    marginLeft: 15
  },
  amount: {
    marginLeft: 15,
    fontSize: 16
  },
  payButton: {
    width: 250,
    alignSelf: 'center',
    marginVertical: 20
  },
  detail: {
    fontSize: 20,
    color: Colors.red900,
    fontWeight: 'bold',
    marginTop: 10,
    marginLeft: 10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  pack: {
    fontSize: 14,
    color: Colors.red700,
    fontWeight: 'bold',
    marginTop: 5,
    marginLeft: 15
  },
  pcs: {
    fontSize: 14,
    color: Colors.gray700,
    fontWeight: 'bold',
    marginLeft: 15
  },
  amountDetail: {

  },
  separator: {
    height: 0.3,
    backgroundColor: Colors.gray700,
    marginTop: 5
  }
});

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from 'utils/ColorReferences';

import OrderList from './OrderList';
import OrderDetail from './OrderDetail';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}
    >
      <Stack.Screen name="OrderList" component={OrderList} />
      <Stack.Screen name="OrderDetail" component={OrderDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;

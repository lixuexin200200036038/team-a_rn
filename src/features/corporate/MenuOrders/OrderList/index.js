import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Modal, FlatList, Alert } from 'react-native';
import { useSelector } from 'react-redux';
import moment from 'moment';

import CardOrderCorporate from 'components/CardOrderCorporate';

import { getOrderCorporate, getPackage, getOrderDetailCorporate } from '../../../../engine/ApiProcessors';

import { styles } from './style';

import { itemStyles } from './itemStyle';

const OrderList = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const [modalOpen, setModalOpen] = useState(false);
  const [orderData, setOrderData] = useState([]);
  const [orderDetailData, setOrderDetailData] = useState([]);
  const [packageData, setPackageData] = useState([]);

  const getData = () => {
    getOrderCorporate(session.token)
      .then((result) => {
        setOrderData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching order data: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getData();
    getPackage(session.token)
      .then((result) => {
        setPackageData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error fetching package data: ${err.toString()}`);
      });
  }, []);

  const openModal = (item) => {
    getOrderDetailCorporate(item.order_id, session.token)
      .then((result) => {
        setOrderDetailData(result.data.data);
        setModalOpen(true);
      })
      .catch((err) => {
        Alert.alert(`Error fetching package data: ${err.toString()}`);
      });
  };

  const renderOrderData = ({ item }) => {
    const packageItem = packageData.find((packageDatum) => packageDatum.package_id === item.package_id_add_number);
    const date = moment(item.createdAt).format('DD MMMM YYYY');

    return (
      <CardOrderCorporate
        Status={item.status}
        Summary={item.order_type}
        date={date}
        Jumlah={item.order_qty}
        Package={packageItem ? packageItem.package_name : ''}
        isAddNumber={item.order_type === 'Add Numbers'}
        onPress={() => openModal(item)}
      />
    );
  };

  const renderChangePackage = ({ item }) => {
    const packageItem = packageData.find((packageDatum) => packageDatum.package_id === item.package_id);

    return (
      <View style={itemStyles.container}>
        <Text style={itemStyles.textRenewal}> {item.msisdn}</Text>

        <Text style={itemStyles.textRenewal}> {item.order_type}</Text>
        <Text style={itemStyles.textRenewal}> Status: {item.status}</Text>
        <View style={itemStyles.container2}>
          <View style={itemStyles.container1}>
            <Text style={itemStyles.status}>
              {' '}
              {packageItem ? packageItem.package_name : ''}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (

    <View style={{ flex: 1 }}>
      <Modal visible={modalOpen} animationType = "slide" transparent>
        <View style={styles.layar}>
          <View style={styles.container}>
            <Text style={styles.textgrey}>Change Packages</Text>
            <TouchableOpacity
              onPress={() => setModalOpen(false)}
            >
              <Text style={styles.textgrey}> X</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={orderDetailData}
            renderItem={renderChangePackage}
            keyExtractor={(_, index) => index.toString()}
          />
        </View>
      </Modal>

      <FlatList
        data={orderData}
        keyExtractor={(_, index) => index.toString()}
        renderItem={renderOrderData}
      />
    </View>
  );
};



export default OrderList;

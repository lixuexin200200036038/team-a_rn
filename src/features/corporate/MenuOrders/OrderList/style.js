import { StyleSheet } from 'react-native';
import { Colors } from 'utils/ColorReferences';

export const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: Colors.gray800
  // },
  label: {
    color: 'white',
    fontWeight: '400',
    fontSize: 38
  },
  layar: {
    backgroundColor: 'white',
    height: '80%',
    marginVertical: 60,
    marginHorizontal: 35,
    width: '80%',
    display: 'flex',
    borderRadius: 20,
    borderWidth: 1
  },
  container: {
    flexDirection: 'row',
    padding: 15,
    textAlign: 'center'
  },
  textgrey:
  {
    color: Colors.gray700,
    fontSize: 21,
    fontWeight: 'bold',
    marginLeft: 30
  },
  textRenewal:
  {
    color: Colors.red700,
    fontSize: 26,
    fontWeight: 'bold'
  },
  container1:
  {
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5,
    width: 90,
    height: 50
  },
  container2: {
    flexDirection: 'row',
    padding: 10,
    borderRadius: 10
  },
  container3: {
    padding: 10,
    marginHorizontal: 5,
    marginVertical: 5,
    backgroundColor: Colors.red700,
    borderWidth: 2,
    borderColor: Colors.red700,
    borderRadius: 5
  }

});

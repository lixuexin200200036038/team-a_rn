import React, { useEffect, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Alert, FlatList } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { useSelector } from 'react-redux';
import CardOrder from 'components/CardOrder/CardOrder';
import moment from 'moment';

import { getOrderAM, approveRejectOrderAM, getPackage } from '../../../../engine/ApiProcessors';

import { styles } from './style';

const OrderAmScreen = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const [initialOrderData, setInitialOrderData] = useState([]);
  const [orderData, setOrderData] = useState([]);
  const [packageData, setPackageData] = useState([]);
  const [searchText, setSearchText] = useState('');

  const OrderDetail = (param) => {
    props.navigation.navigate('OrderDetail', {
      orderId: param
    });
  };

  const getData = () => {
    getOrderAM(session.token)
      .then((result) => {
        setInitialOrderData(result.data.data);
        setOrderData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  };

  useEffect(() => {
    getData();

    getPackage(session.token)
      .then((result) => {
        setPackageData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  }, []);

  const approveRejectOrder = (isApprove, orderId) => {
    const data = { status: isApprove ? 'completed' : 'canceled', order_id: orderId };

    approveRejectOrderAM(data, session.token)
      .then((result) => {
        Alert.alert(`Order ${isApprove ? 'approved' : 'rejected'} successfully.`);
        getData();
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  };

  const renderCardOrder = ({ item }) => {
    const packageItem = packageData.find((packageDatum) => packageDatum.package_id === item.package_id_add_number);

    return (
      <TouchableOpacity onPress={() => OrderDetail(item.order_id)} disabled={item.order_type === 'Add Numbers'}>
        <CardOrder
          Status={item.status}
          Renewal={item.order_type}
          date={moment(item.createdAt).format('DD MMMM YYYY')}
          judul="PIC"
          Nama={`Nama : ${item.name}`}
          Email={`Email : ${item.email}`}
          Phone={item.phone}
          Corporate={item.account_name}
          addNumber={item.order_type === 'Add Numbers' && item.status === 'open'}
          quantity={item.order_qty}
          packageName={packageItem ? packageItem.package_name : ''}
          onPressApprove={() => approveRejectOrder(true, item.order_id)}
          onPressReject={() => approveRejectOrder(false, item.order_id)}
        />
      </TouchableOpacity>
    );
  };

  const searchOrders = (text) => {
    setSearchText(text);
    if (text) {
      setOrderData(initialOrderData.filter((orderItem) => {
        return orderItem.account_name.search(text) >= 0;
      }));
    } else {
      setOrderData(initialOrderData);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.header}>
        <SearchBar
          onChangeText={(text) => searchOrders(text)}
          placeholder="Search Corporate Name"
          value={searchText}
          onClear={() => setOrderData(initialOrderData)}
        />
      </View>

      <FlatList
        data={orderData}
        renderItem={renderCardOrder}
        keyExtractor={(item) => item.order_id.toString()}
      />
    </View>
  );
};

export default OrderAmScreen;

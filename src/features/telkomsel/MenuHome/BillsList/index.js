import React from 'react';
import { Text, View, TouchableOpacity, FlatList, Alert } from 'react-native';

import { Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { styles } from './style';

const BillsAM = () => {
  return (
    <View style={styles.mainPage}>
      <Header
        leftComponent={{
          icon: 'arrow-back',
          onPress: () => {
            Alert.alert('Back');
          }
        }}
        centerComponent={{
          text: 'Bills',
          style: styles.textHeader
        }}
        containerStyle={styles.containerHeader}
      />
      <View style={styles.total}>
        <View style={styles.totalBill}>
          <Text style={styles.textTotalBill}>Total Bill</Text>
          <Text style={styles.amountTotalBill}>Rp. 1.700.000</Text>
        </View>
        <View style={styles.totalOutstanding}>
          <Text style={styles.textTotalBill}>Total</Text>
          <Text style={styles.textOutstanding}>Outstanding</Text>
          <Text style={styles.amountOutstanding}>Rp. 700.000</Text>
        </View>
      </View>

      <FlatList
        data={[
          { name: 'satu' },
          { name: 'dua' },
          { name: 'tiga' },
          { name: 'empat' },
          { name: 'lima' },
          { name: 'enam' }
        ]}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={(item) => {
          // return <CardBillsAM item={item} />;
        }}
      />
      <TouchableOpacity style={styles.filterButton}>
        <Icon
          name="filter-variant"
          style={styles.iconFilter}
          size={25}
        />
        <Text style={styles.textFilter}>Filter</Text>
      </TouchableOpacity>
    </View>
  );
};

export default BillsAM;

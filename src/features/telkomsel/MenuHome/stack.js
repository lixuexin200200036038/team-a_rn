import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from 'utils/ColorReferences';

import ChatDetail from 'features/both/Chats/ChatDetail/web';

import ChatList from 'features/both/Chats/ChatList';

import TicketDetail from '../../both/Tickets/TicketDetail';

import Home from './Home';
import BillsList from './BillsList';
import CorporateBillsDetail from './../MenuCorporates/CorporateBillsDetail';


const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}
    >
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="BillsList" component={BillsList} />
      <Stack.Screen name="CorporateBillsDetail" component={CorporateBillsDetail} />
      <Stack.Screen name="TicketDetail" component={TicketDetail} />
      <Stack.Screen name="ChatList" component={ChatList} />
      <Stack.Screen name="ChatDetail" component={ChatDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;

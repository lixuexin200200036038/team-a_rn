import React from 'react';
import { Text, View, Alert } from 'react-native';

import { Header } from 'react-native-elements';

import { styles } from './style';

const BillDetailAM = () => {
  return (
    <View style={styles.mainPage}>
      <Header
        leftComponent={{
          icon: 'arrow-back',
          onPress: () => {
            Alert.alert('Back');
          }
        }}
        centerComponent={{
          text: 'Invoice Tsel-101/II/2021',
          style: styles.textHeader
        }}
        containerStyle={styles.containerHeader}
      />

      <Text style={styles.summary}>Summary</Text>
      <Text style={styles.status}>Status: Paid</Text>
      <Text style={styles.month}>Feb 2021</Text>
      <Text style={styles.company}>PT. Fakhaza Healthy Kitchen</Text>
      <Text style={styles.amount}>Total Rp. 1.700.000</Text>
      <Text style={styles.detail}>Details</Text>
      <View style={styles.row}>
        <View>
          <Text style={styles.pack}>OMG! 10GB</Text>
          <Text style={styles.pcs}>10 pcs</Text>
        </View>
        <Text style={styles.amountDetail}>Rp. 1.700.000</Text>
      </View>
      <View style={styles.separator} />
      <View style={styles.row}>
        <View>
          <Text style={styles.pack}>OMG! 20GB</Text>
          <Text style={styles.pcs}>20 pcs</Text>
        </View>
        <Text style={styles.amountDetail}>Rp. 1.700.000</Text>
      </View>
      <View style={styles.separator} />
      <View style={styles.row}>
        <View>
          <Text style={styles.pack}>OMG! 30GB</Text>
          <Text style={styles.pcs}>30 pcs</Text>
        </View>
        <Text style={styles.amountDetail}>Rp. 1.700.000</Text>
      </View>
      <View style={styles.separator} />
    </View>
  );
};

export default BillDetailAM;

import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from 'utils/ColorReferences';

import TicketDetail from '../../both/Tickets/TicketDetail';

import CorporateList from './CorporateList';
import CorporateDetail from './CorporateDetail';
import CorporateBillsDetail from './CorporateBillsDetail';
import PopUpChangePacket from './PopUpChangePacket';
import PopUpCreateNewNumber from './PopUpCreateNewNumber';


const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}
    >
      <Stack.Screen name="CorporateList" component={CorporateList} />
      <Stack.Screen name="CorporateDetail" component={CorporateDetail} />
      <Stack.Screen
        name="CorporateBillsDetail"
        component={CorporateBillsDetail}
      />
      <Stack.Screen name="PopUpChangePacket" component={PopUpChangePacket} />
      <Stack.Screen
        name="PopUpCreateNewNumber"
        component={PopUpCreateNewNumber}
      />
      <Stack.Screen name="TicketDetail" component={TicketDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;

import React, { useEffect, useState } from 'react';
import { View, Alert, FlatList } from 'react-native';
import { SearchBar } from 'react-native-elements';
import { useSelector } from 'react-redux';

import CardCorporate from 'components/CardCorporate/CardCorporate';

import { getCorporateAM } from '../../../../engine/ApiProcessors';

import { styles } from './style';

const CorporateAmScreen = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const [corporateData, setCorporateData] = useState([]);
  const [initialCorporateData, setInitialCorporateData] = useState([]);
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    getCorporateAM(session.token)
      .then((result) => {
        setInitialCorporateData(result.data.data);
        setCorporateData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(err.toString());
      });
  }, []);

  const renderCardCorporate = ({ item }) => {
    return (
      <CardCorporate
        judul="PIC"
        Nama={`Nama : ${item.name}`}
        Email={`Email : ${item.email}`}
        Phone={`Phone : ${item.phone}`}
        Corporate={item.account_name}
        gambar={item.avatar}
        onPress={() => props.navigation.navigate('CorporateDetail', { item })}
      />
    );
  };

  const searchCorporate = (text) => {
    setSearchText(text);
    if (text) {
      setCorporateData(initialCorporateData.filter((orderItem) => {
        return orderItem.account_name.search(text) >= 0;
      }));
    } else {
      setCorporateData(initialCorporateData);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.header}>
        <SearchBar
          onChangeText={(text) => searchCorporate(text)}
          placeholder="Search Corporate Name"
          value={searchText}
          onClear={() => setCorporateData(initialCorporateData)}
        />
      </View>
      <FlatList
        data={corporateData}
        renderItem={renderCardCorporate}
        keyExtractor={(item) => item.account_id.toString()}
      />
    </View>
  );
};

export default CorporateAmScreen;

import { Image, ImageBackground } from 'react-native';
import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';

import { TextInput } from 'react-native-paper';
import { Colors } from 'utils/ColorReferences';
import Button from 'components/Button/Button';
import { useSelector, useDispatch } from 'react-redux';
import { reduxLogin } from 'reduxs/actions/AuthAction';
import {
  setSharedPreferencesJson,
  getSharedPreferencesJson
} from 'engine/SharedPreferences';
import { Images } from 'utils/ImageReferences';

import { styles } from './style';

const LoginScreen = (props) => {
  const { session, isLoading } = useSelector((state) => state.AuthReducer);
  const dispatch = useDispatch();
  const doLogin = (param) => dispatch(reduxLogin(param));

  const [form, setForm] = useState({
    email: '',
    password: ''
  });

  const getTextInputStyle = () => {
    return {
      colors: {
        placeholder: Colors.gray700,
        text: Colors.red700,
        primary: Colors.red700,
        underlineColor: Colors.red700
      }
    };
  };

  const OnInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value
    });
  };

  useEffect(() => {
    getSharedPreferencesJson('@login', {
      email: 'user_demo_am@telkomsel.co.id',
      password: 'Test1234'


    })
      .then((res) => {
        // console.log(res);
        setForm(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  useEffect(() => {
    // console.log(session);
    if (session.isLoggedIn === true && session.role === 'telkomsel') {
      props.navigation.reset({
        index: 0,
        routes: [{ name: 'TselBottomNavigator' }]
      });
      setSharedPreferencesJson('@login', form);
    } else if (session.isLoggedIn === true && session.role === 'corporate') {
      props.navigation.reset({
        index: 0,
        routes: [{ name: 'CorpBottomNavigator' }]
      });
      setSharedPreferencesJson('@login', form);
    }
  }, [session]);

  const [prevIsLoading, setPrevIsLoading] = useState(isLoading);
  useEffect(() => {
    if (prevIsLoading !== isLoading) {
      if (prevIsLoading === true && isLoading === false) {
      }
      setPrevIsLoading(isLoading);
    }
  }, [isLoading]);

  return (
    <ImageBackground style={styles.image} source={Images.backgroundlogin}>
      <View style={styles.container}>
        <View style={styles.container}>
          <Image style={styles.logo} source={Images.logo} />
          <Text style={styles.judul}>Digital Smart Care</Text>
          <View>
            <View style={styles.button}>
              <TextInput
                label="Email"
                mode="outlined"
                theme={getTextInputStyle()}
                value={form.email}
                onChangeText={(text) => OnInputChange(text, 'email')}
              />
              <TextInput
                style={styles.form}
                label="Password"
                mode="outlined"
                secureTextEntry={true}
                theme={getTextInputStyle()}
                value={form.password}
                onChangeText={(text) => OnInputChange(text, 'password')}
              />
              <View style={{ marginTop: 30 }}>
                <Button
                  title="Sign in"
                  onPress={() => {
                    let param = form;
                    doLogin(param);
                  }}
                />
              </View>
            </View>
          </View>
        </View>
      </View>
    </ImageBackground>
  );
};

export default LoginScreen;

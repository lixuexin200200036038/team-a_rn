import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, Alert, TextInput } from 'react-native';
import { useSelector } from 'react-redux';

import { Button } from 'react-native-elements';

import { getTicketDetailAM, replyTicketAM } from '../../../../engine/ApiProcessors';

import { styles } from './style';


const TicketDetailAM = (props) => {
  const { session } = useSelector((state) => state.AuthReducer);
  const { ticket } = props.route.params;

  const [ticketDetailData, setTicketDetailData] = useState({ history_reply: [] });
  const [replyText, setReplyText] = useState('');

  const getTicketDetailData = () => {
    getTicketDetailAM(ticket.ticket_id, session.token)
      .then((result) => {
        setTicketDetailData(result.data.data);
      })
      .catch((err) => {
        Alert.alert(`Error getting ticket data: ${err.toString()}`);
      });
  };

  useEffect(() => {
    getTicketDetailData();
  }, []);

  const renderHistoryReply = ({ item }) => {
    return (
      <View style={item.office_name === 'Telkomsel' ? styles.rev2 : styles.rev1}>
        <Text style={item.office_name === 'Telkomsel' ? styles.textRev2 : styles.textRev1}>
          {item.comments}
        </Text>
      </View>
    );
  };

  const sendReply = (text) => {
    const data = {
      ticket_id: ticket.ticket_id,
      comments: text
    };

    replyTicketAM(data, session.token)
      .then((result) => {
        getTicketDetailData();
        setReplyText('');
      })
      .catch((err) => {
        Alert.alert(`Error replying ticket: ${err.toString()}`);
      });
  };

  return (
    <View style={styles.mainPage}>
      <View style={styles.cardTickets}>
        <Text style={styles.company}>{ticket.company_name}</Text>
        <Text style={styles.status}>Status: {ticket.status}</Text>
        <Text style={styles.category}>{ticket.ticket_category}</Text>
        <Text style={styles.content}>
          {ticket.comment}
        </Text>
      </View>

      <View style={styles.separator} />

      <FlatList
        data={ticketDetailData.history_reply}
        renderItem={renderHistoryReply}
        keyExtractor={(item) => item.createdAt}
      />

      <View style={styles.detail}>
        <TextInput value={replyText} onChangeText={(text) => setReplyText(text)} />
      </View>
      <View style={styles.separator} />

      <View style={styles.button}>
        <Button
          title="Reply"
          onPress={() => {
            sendReply(replyText);
          }}
          containerStyle={{}}
        />
      </View>
    </View>
  );
};

export default TicketDetailAM;

import React, { Component } from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  ScrollView,
  RefreshControl,
  View,
  Text,
  Image,
  Alert
} from 'react-native';
import { connect } from 'react-redux';
import { getChatList } from 'engine/ApiProcessors';

class ChatScreen extends Component {
  constructor() {
    super();
    this.state = {
      data: null,
      token: this,
      refreshing: false
    };
  }

  // fetchNews = async () => {
  //   try {
  //     let urls = [
  //       'http://newsapi.org/v2/everything?q=bitcoin&from=2020-12-10&sortBy=publishedAt&apiKey=dbabf802dd6d47c390438b4b0bc76a77',
  //       'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=dbabf802dd6d47c390438b4b0bc76a77',
  //       'http://newsapi.org/v2/everything?q=apple&from=2021-01-09&to=2021-01-09&sortBy=popularity&apiKey=dbabf802dd6d47c390438b4b0bc76a77',
  //       'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=dbabf802dd6d47c390438b4b0bc76a77',
  //       'http://newsapi.org/v2/everything?domains=wsj.com&apiKey=dbabf802dd6d47c390438b4b0bc76a77',
  //     ];
  //     let res = await axios.get(urls[Math.floor(Math.random() * urls.length)]);
  //     this.setState({data: res.data});
  //   } catch (err) {
  //     Alert.alert('Error', err.message);
  //   }
  // };

  async componentDidMount() {
    // console.log(this.props.session.token);
    this.setState({ token: this.props.session.token, refreshing: true });
    try {
      let res = await getChatList(this.props.session.token);
      // console.log(res.data.data);
      this.setState({ data: res.data.data, refreshing: false });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <View>
        <ScrollView
          style={styles.scrollView}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => {
                getChatList(this.props.session.token)
                  .then((r) => {
                    this.setState({ data: r.data.data, refreshing: false });
                  })
                  .catch((e) => {
                    Alert.alert('Damn!', `Error: ${e.message}`);
                  });
              }}
            />
          }
        >
          {this.state.data &&
            this.state.data.map((item, i) => {
              {
                /* console.log(i, item); */
              }
              try {
                return (
                  <TouchableHighlight
                    underlayColor="#fefefe00"
                    onPress={() => {
                      let url = `http://139.59.124.53:4581/api/chat?username=${this.props.session.data.email}&room=${item.email}`;
                      this.props.navigation.navigate('ChatDetail', `${url}`);
                    }}
                    key={i}
                  >
                    <View style={styles.container}>
                      <Image
                        style={styles.images}
                        source={{ uri: item.avatar }}
                      />
                      <View style={styles.titlesView}>
                        <Text style={styles.titles}>{`${item.name}`}</Text>
                        <Text
                          style={styles.subtitles}
                        >{`${item.account_name}\n${item.email}`}</Text>
                      </View>
                    </View>
                  </TouchableHighlight>
                );
              } catch (err) {
                Alert.alert('Damn!', `Error: ${err.message}`);
              }
            })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#fefefe',
    height: '100%'
  },
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    padding: 20
    // borderBottomWidth: 0.25,
    // borderBottomColor: '#bcbcbc',
    // backgroundColor: '#fefefe',
  },
  images: {
    width: 60,
    height: 60,
    padding: 18,
    borderRadius: 1000
  },
  titlesView: {
    flex: 1,
    marginLeft: 14,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  titles: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  subtitles: {
    fontSize: 16,
    marginTop: 4,
    marginBottom: 4,
    fontWeight: '300'
  },
  adminContainer: {
    flexDirection: 'row',
    paddingBottom: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: '#ababab',
    backgroundColor: '#fefefe'
  },
  adminView: {
    marginLeft: 20,
    paddingTop: 30,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  iconAdmin: {
    width: 44,
    height: 44,
    marginTop: 44,
    borderRadius: 100
  },
  adminTitle: {
    marginTop: 44,
    fontSize: 18,
    paddingTop: 10,
    marginLeft: 20,
    fontWeight: 'bold',
    color: 'white'
  }
});

const mapStateToProps = (state) => ({
  session: state.AuthReducer.session
});

const mapDispatchToProps = () => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps())(ChatScreen);

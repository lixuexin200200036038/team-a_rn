import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from 'utils/ColorReferences';

import ChatList from './ChatList';
import ChatDetail from './ChatDetail';

const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerBackTitle: '',
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.red700,
          shadowColor: '#fff0'
        }
      }}>
      <Stack.Screen name="ChatList" component={ChatList} />
      <Stack.Screen name="ChatDetail" component={ChatDetail} />
    </Stack.Navigator>
  );
};

export default StackNavigator;

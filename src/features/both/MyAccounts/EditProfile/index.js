import React from 'react';
import {
  ScrollView,
  KeyboardAvoidingView,
  View,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  Image,
  Platform
} from 'react-native';
import Button from 'components/Button/Button';

import ImagePicker from 'react-native-image-crop-picker';
import { TextInput } from 'react-native-paper';
import { useSelector, useDispatch } from 'react-redux';
import { useEffect, useState, useRef } from 'react';
import { Colors } from 'utils/ColorReferences';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomSheet from 'reanimated-bottom-sheet';
import { reduxUpdateProfile } from 'reduxs/actions/AuthAction';
import {
  setSharedPreferencesJson,
  getSharedPreferencesJson
} from 'engine/SharedPreferences';

import { styles } from './style';

const EditProfile = (props) => {
  const { session, isLoading } = useSelector((state) => state.AuthReducer);
  const dispatch = useDispatch();

  const doUpdateProfile = (role, param, headers) =>
    dispatch(reduxUpdateProfile(role, param, headers));

  const [form, setForm] = useState({
    avatar: session.data.avatar,
    email: session.data.email,
    name: session.data.name,
    nik: session.data.nik,
    job_position: session.data.job_position,
    phone: session.data.phone,
    workplace: session.data.workplace,
    password: ''
  });

  const getTextInputStyle = () => {
    return {
      colors: {
        placeholder: Colors.gray700,
        text: Colors.black,
        primary: Colors.red700,
        underlineColor: Colors.red700
      }
    };
  };

  const OnInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value
    });
  };

  const [prevIsLoading, setPrevIsLoading] = useState(isLoading);
  useEffect(() => {
    if (prevIsLoading !== isLoading) {
      if (prevIsLoading === true && isLoading === false) {
        setSharedPreferencesJson('@login', {
          email: form.email,
          password: form.password
        });
        props.navigation.pop();
      }
      setPrevIsLoading(isLoading);
    }
  }, [isLoading]);

  useEffect(() => {
    getSharedPreferencesJson('@login', {
      email: 'user_demo_am@telkomsel.co.id',
      password: 'Test1234'
    })
      .then((res) => {
        // res = JSON.parse(res);
        // console.log(res.email);
        OnInputChange(res.password, 'password');
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const sheetRef = useRef(null);
  const renderContent = () => (
    <View
      style={{
        backgroundColor: Colors.white,
        padding: 30,
        height: 180,
        width: '100%',
        borderTopLeftRadius: 14,
        borderTopRightRadius: 14
      }}
    >
      <Text
        style={{
          fontWeight: 'bold',
          fontSize: 18
        }}
      >
        Choose Image From:
      </Text>
      <View
        style={{
          flex: 1,
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-around'
        }}
      >
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={() => fromCamera()}
        >
          <Icon
            name="camera"
            size={60}
            color={Colors.red700}
            style={{
              paddingTop: 30
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.5}
          style={styles.button}
          onPress={() => fromLibrary()}
        >
          <Icon
            name="image"
            size={60}
            color={Colors.red700}
            style={{
              paddingTop: 30
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );

  // const [fileData, setFileData] = useState('');
  // const [fileUri, setFileUri] = useState('');

  const fromCamera = () => {
    ImagePicker.openCamera({
      mediaType: 'photo',
      compressImageMaxWidth: 600,
      compressImageMaxHeight: 600,
      cropping: true,
      includeBase64: true
    })
      .then((image) => {
        console.log(`data:image/jpeg;base64,${image.data}`);
        // setFileData(image.data);
        // setFileUri(image.sourceURL);
        setForm({
          ...form,
          avatar: `data:image/jpeg;base64,${image.data}`
        });
        sheetRef.current.snapTo(2);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const fromLibrary = () => {
    ImagePicker.openPicker({
      mediaType: 'photo',
      compressImageMaxWidth: 600,
      compressImageMaxHeight: 600,
      cropping: true,
      includeBase64: true
    })
      .then((image) => {
        // console.log(image);
        // setFileData(image.data);
        // setFileUri(image.sourceURL);
        setForm({
          ...form,
          avatar: `data:image/jpeg;base64,${image.data}`
        });
        sheetRef.current.snapTo(2);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <>
      <ScrollView
        style={[styles.scrollView]}
        contentContainerStyle={styles.scrollViewContainer}
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="interactive"
      >
        <View style={styles.mainPage}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={styles.mainPage}
          >
            <View style={[styles.topPage]} />
            <View style={[styles.bottomPage]}>
              <View>
                {/* <Image source={Images.profile} style={styles.profilePicture} /> */}
                <Image
                  source={{ uri: form && form.avatar }}
                  style={styles.profilePicture}
                />
                <TouchableHighlight
                  onPress={() => {
                    sheetRef.current.snapTo(0);
                    // fromLibrary();
                  }}
                  activeOpacity={0.7}
                  style={styles.profilePictureEdit}
                >
                  <Icon name="pencil" size={32} color={Colors.white} />
                </TouchableHighlight>
              </View>
              <View style={styles.profileContainer}>
                <Text style={styles.profileEmail}>
                  {session.data && session.data.email}
                </Text>
              </View>
              <View style={styles.profileContainer}>
                <View style={styles.profileRow}>
                  <TextInput
                    label="Nama"
                    mode="outlined"
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.name}
                    onChangeText={(text) => OnInputChange(text, 'name')}
                  />
                </View>
                <View style={styles.profileRow}>
                  <TextInput
                    label="NIK"
                    mode="outlined"
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.nik}
                    onChangeText={(text) => OnInputChange(text, 'nik')}
                  />
                </View>
                <View style={styles.profileRow}>
                  <TextInput
                    label="Title"
                    mode="outlined"
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.job_position}
                    onChangeText={(text) => OnInputChange(text, 'job_position')}
                  />
                </View>
                <View style={styles.profileRow}>
                  <TextInput
                    label="Phone Number"
                    mode="outlined"
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.phone}
                    onChangeText={(text) => OnInputChange(text, 'phone')}
                  />
                </View>
                <View style={styles.profileRow}>
                  <TextInput
                    label="Workplace"
                    mode="outlined"
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.workplace}
                    onChangeText={(text) => OnInputChange(text, 'workplace')}
                  />
                </View>
                <View style={styles.profileRow}>
                  <TextInput
                    label="Password"
                    mode="outlined"
                    secureTextEntry={true}
                    style={styles.profileRow}
                    theme={getTextInputStyle()}
                    value={form.password}
                    onChangeText={(text) => OnInputChange(text, 'password')}
                  />
                </View>
                <View style={styles.buttonSave}>
                  <Button
                    title="Save"
                    onPress={() => {
                      // console.log(session.token);
                      doUpdateProfile(session.role, form, session.token);
                    }}
                  />
                </View>
              </View>
            </View>
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
      <BottomSheet
        ref={sheetRef}
        initialSnap={2}
        snapPoints={[180, 180, 0]}
        borderRadius={10}
        renderContent={renderContent}
      />
    </>
  );
};

export default EditProfile;
